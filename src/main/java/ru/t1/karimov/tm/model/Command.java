package ru.t1.karimov.tm.model;

import ru.t1.karimov.tm.constant.ArgumentConst;

import ru.t1.karimov.tm.constant.CommandConst;

public class Command {

    public static final Command ABOUT = new Command(CommandConst.ABOUT, ArgumentConst.ABOUT,"Show about program.");

    public static final Command VERSION = new Command(CommandConst.VERSION, ArgumentConst.VERSION,"Show program version.");

    public static final Command HELP = new Command(CommandConst.HELP, ArgumentConst.HELP,"Show list arguments.");

    public static final Command INFO = new Command(CommandConst.INFO, ArgumentConst.INFO,"Show system information.");

    public static final Command EXIT = new Command(CommandConst.EXIT, null,"Close application.");

    private String name;

    private String argument;

    private String description;

    public Command() {
    }

    public Command(String name, String argument, String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String displayName = "";
        if(name != null && !name.isEmpty()) displayName += name;
        if(argument != null && !argument.isEmpty()) displayName += ", " + argument;
        if(description != null && !description.isEmpty()) displayName += ": " + description;
        return displayName;
    }

}

package ru.t1.karimov.tm;

import ru.t1.karimov.tm.constant.ArgumentConst;

import ru.t1.karimov.tm.constant.CommandConst;
import ru.t1.karimov.tm.model.Command;
import ru.t1.karimov.tm.util.FormatUtil;

import static ru.t1.karimov.tm.constant.CommandConst.*;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
            break;
            case ArgumentConst.VERSION:
                showVersion();
            break;
            case ArgumentConst.HELP:
                showHelp();
            break;
            case ArgumentConst.INFO:
                showInfo();
            break;
            default:
                showArgumentError();
            break;
        }
    }

    private static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.ABOUT:
                showAbout();
            break;
            case CommandConst.VERSION:
                showVersion();
            break;
            case CommandConst.HELP:
                showHelp();
            break;
            case CommandConst.INFO:
                showInfo();
            break;
            case CommandConst.EXIT:
                exit();
            break;
            default:
                showCommandError();
            break;
        }
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument not support...");
        System.exit(1);
    }

    private static void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This command not support...");
        System.exit(1);
    }

    private static void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + FormatUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = maxMemoryCheck ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + FormatUtil.formatBytes(totalMemory));
        final long usegeMemory = totalMemory - freeMemory;
        System.out.println("Usage memory: " + FormatUtil.formatBytes(usegeMemory));
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Alfred Karimov");
        System.out.println("e-mail: alfred@karimov.ru");
        System.out.println("e-mail: alfred_test@karimov.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.5.0");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(Command.ABOUT);
        System.out.println(Command.VERSION);
        System.out.println(Command.HELP);
        System.out.println(Command.INFO);
        System.out.println(Command.EXIT);
    }

}
